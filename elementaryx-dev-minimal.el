;;; ElementaryX: Elementary Emacs configuration coupled with Guix
;;;
;;; Extra config: Development tools

;;; Usage: Append or require this file from init.el for some software
;;; development-focused packages.
;;;
;;; It is **STRONGLY** recommended that you use the base.el config if you want to
;;; use Eglot. Lots of completion things will work better.
;;;
;;; This will try to use tree-sitter modes for many languages. Please run
;;;
;;;   M-x treesit-install-language-grammar
;;;
;;; Before trying to use a treesit mode.

;;; Contents:
;;;
;;;  - Built-in config for developers
;;;  - Version Control
;;;  - Common file types
;;;  - Eglot, the built-in LSP client for Emacs

(use-package elementaryx-base)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;    Project Roots
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Define a customizable variable `elementaryx-project-roots`.
;; This variable holds a list of directories (paths) where Emacs will recursively look for projects.
;; By default, it is set to a list containing the single directory "~/project/".
(defcustom elementaryx-project-roots '("~/project/")
  "List of directories considered as project roots.
Each directory in this list is a path where Emacs will search for projects recursively. This list is used:
- by `elementaryx-update-project-roots' to update the list of projects `project.el' is aware of;
- to set up `magit-repository-directories' used by`magit-repos'"
  :type '(repeat directory)  ;; The variable type is a repeatable list of directories.
  :group 'elementaryx)      ;; Associate this variable with the 'elementaryx' customization group.

;; Note that `elementaryx-update-project-roots' internally relies on
;; `project-remember-projects-under' which can be called either
;; recursively or not. Regarding`magit-repository-directories', it can
;; be set up with a finer control on the depth of the recursion. This
;; depth is set up to 6 by default and can be overriden.
(defcustom elementaryx-magit-repository-depth 6
  "Default depth for searching repositories in `magit-repository-directories`."
  :type 'integer
  :group 'elementaryx)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Version Control
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; Highlight VC differences: the two main emacs packages are currently diff-hl and git-gutter.

;; diff-hl (guix emacs-diff-hl package)
;; diff-hl-flydiff-mode uses the external diff (from guix diffutils package)
;; There's no fringe when Emacs is running in the console, but the navigation and revert commands still work. Consider turning diff-hl-margin-mode on, to show the indicators in the margin instead.
(use-package diff-hl
  :demand t
  :hook ((dired-mode . diff-hl-dired-mode)
         (magit-pre-refresh . diff-hl-magit-pre-refresh)
         (magit-post-refresh . diff-hl-magit-post-refresh))
  :config
  (global-diff-hl-mode)    ;; Enable diff-hl-mode globally
  (unless (display-graphic-p)    ;; Enable margin mode only in terminal
    (diff-hl-margin-mode))
  (diff-hl-flydiff-mode))  ;; Turn on real-time diff updates)

;; (require 'diff-hl)
;; (global-diff-hl-mode)
;; (add-hook 'magit-pre-refresh-hook 'diff-hl-magit-pre-refresh)
;; (add-hook 'magit-post-refresh-hook 'diff-hl-magit-post-refresh)

;; ;; git-gutter (guix emacs-git-gutter package)
;; (use-package git-gutter
;;   :config
;;   (global-git-gutter-mode +1))

;; Magit: best Git client to ever exist
(use-package magit
  :commands (magit-project-status magit-init)
  :bind (("C-x g" . magit-status)
	 ("C-c g g" . magit-status)
	 ("C-c g i" . magit-init))
  :init
  (which-key-add-key-based-replacements "C-c g" "git")
  :custom
  (magit-diff-refine-hunk 'all)) ;; show word-granularity differences within all diff hunks
                                 ;; use `t' instead of `'all' if observing performance issues in diffhttps://magit.vc/manual/magit/Performance.html

;; magit-repos
(use-package magit-repos
  :commands (magit-list-repositories)
  :bind (("C-c g L" . magit-list-repositories))

  :config

  (defun elementaryx-convert-to-magit-repos (project-roots &optional depth)
  "Convert PROJECT-ROOTS to a list suitable for `magit-repository-directories`.
DEPTH specifies how many levels deep Magit should search for repositories.
If DEPTH is not provided, it defaults to 0."
  (let ((depth (or depth 0)))  ;; Default to 0 if depth is not provided
    (mapcar (lambda (dir) (cons dir depth)) project-roots)))

  (setq magit-repository-directories
      (elementaryx-convert-to-magit-repos elementaryx-project-roots elementaryx-magit-repository-depth)))

  ;; :custom
  ;; (magit-repository-directories '(("~/project/" . 6))))

;; TODO emacs-git-timemachine

;; TODO emacs-git-blame

;; TODO https://github.com/armindarvish/consult-gh (consult GitHub CLI)

;; TODO (project-remember-projects-under "~/project/" t)

;; TODO  https://docs.projectile.mx/projectile/configuration.html#cmake
;; (setq projectile-enable-cmake-presets t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;  Project management
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; We rely on built-in `project.el' for project management. `project.el'
;; discovers dynamically (and tracks) projects as you visist them.

;; In addition, ElementaryX automatically may automatically update
;; project list by recursively searching for projects in
;; `elementaryx-project-roots' roots. This variable may be customized.

(use-package project
  :defer t

  :bind (("C-c e o" . elementaryx-edit-envrc))

  :init

  ;; Define a function that scans and remembers all projects located under the directories
  ;; listed in `elementaryx-project-roots`.
  (defun elementaryx-update-project-roots ()
    "Remember all projects under `elementaryx-project-roots and forget all known projects that don't exist anymore. WARNING: this function may take a while; go and grab a coffee."
    (interactive)  ;; Make this function callable interactively via M-x.

    ;; Print WARNING
    (message "WARNING: This command may take a few minutes to re-index your projects. Go and grab a coffee. Or press `C-g' to cancel.")

    ;; Loop over each directory in `elementaryx-project-roots`.
    (dolist (root elementaryx-project-roots)
      ;; Check if the directory exists.
      (when (file-directory-p root)
	;; Recursively find and remember all projects under the directory using `project-remember-projects-under`.
	;; This makes Emacs aware of projects in these locations.
	(project-remember-projects-under root t)))

    ;; Clean up: Forget all known projects that don’t exist any more.
    (project-forget-zombie-projects))

  ;; Define a function to edit the .envrc at the root of the project
  (defun elementaryx-edit-envrc ()
    "Open the .envrc file at the root of the current project.
Create an empty .envrc file if none exists. Display an error if not in a project."
    (interactive)
    (let* ((project (project-current))
           (root (if project (project-root project))))
      (if root
          (let ((envrc-path (expand-file-name ".envrc" root)))
            (find-file envrc-path))  ;; Opens or creates the .envrc file
	(message "Not inside a project!"))))

  ;; Define a Project Menu (in addition and independently from the one in Tools)

  (defun elementaryx-in-project-p ()
    "Return t if the current buffer is in a project."
    (when (fboundp 'project-current)
      (project-current)))

  (easy-menu-define
    elementaryx-project-menu
    nil
    "Menu for Project Management."
    '("Project"
      ["Switch Project" project-switch-project t]
      "---" ;; Separator
      ["Treemacs Add" treemacs-add-and-display-current-project :visible (elementaryx-in-project-p) :help "Add current Project to Treemacs side bar"]
      ["Treemacs Set" treemacs-add-and-display-current-project-exclusively :visible (elementaryx-in-project-p) :help "Set current Project as the unique project within Treemacs side bar"]
      ["Treemacs Toggle" treemacs :help "Toggle Treemacs side bar"]
      ["Treemacs Remove" treemacs-remove-project-from-workspace :help "Remove current Project from Treemacs side bar"]
      "---"
      ["Search with Rip Grep" consult-ripgrep :visible (elementaryx-in-project-p) :help "Search with Rip Grep within current Project"] ;; TODO: fix and use elementaryx-consult-ripgrep
      ["Search with Git Grep" consult-git-grep :visible (elementaryx-in-project-p) :help "Search with Git Grep within current Project"] ;; TODO: fix and use elementaryx-consult-git-grep
      ["Basic Query Replace" project-query-replace-regexp :visible (elementaryx-in-project-p) :help "Basic built-in Emacs Query Replace within current Project"]
      ["Consult Buffers" consult-project-buffer :visible (elementaryx-in-project-p) :help "Consult current Project Buffers"]
      ["Kill Buffers" project-kill-buffers :visible (elementaryx-in-project-p) :help "Kill current Project Buffers"]
      ["Consult Marks" elementaryx-consult-project-marks :visible (elementaryx-in-project-p) :help "Consult Marks within current Project"]
      ["Find File" project-find-file :visible (elementaryx-in-project-p) :help "Find File within current Project"]
      ["Find Directory" project-find-dir :visible (elementaryx-in-project-p) :help "Find Directory within current Project"]
      "---"
      ["Terminal (vterm)" multi-vterm-project :visible (elementaryx-in-project-p) :help "Toggle Terminal associated with current Project"]
      "---"
      ("Environment (Direnv)" :visible (elementaryx-in-project-p)
       ["Edit .envrc" elementaryx-edit-envrc :help "Open the .envrc file at the root of the current project"]
       "---"
       ["Allow" envrc-allow :help "Run `direnv allow'"]
       ["Deny" envrc-deny :help "Run `direnv deny'"]
       ["Reload" envrc-reload :help "Reload the environment"]
       "---"
       ["Show Log" envrc-show-log :help "Show direnv Log"]
       )
      ["Build" build-menu :visible (elementaryx-in-project-p) :help "Build Project" ]
      ["Language Server Protocol (Eglot)" eglot :visible (elementaryx-in-project-p) :help "Start Language Server Protocol (LSP) server in support of PROJECT’s buffers under MANAGED-MAJOR-MODE"]
      ["Compiler Explorer (RMSBolt)" rmsbolt-mode :visible (elementaryx-in-project-p) :help "Toggle RMSBolt, a supercharged implementation of the Godbolt Compiler-Explorer"]
      "---"
      ["Git management (Magit)" magit-status :visible (elementaryx-in-project-p) :help "Open Git management of the current Project (through Magit)"]
      "---"
      ["Update Projects List" elementaryx-update-project-roots t :help "Remember all projects under `elementaryx-project-roots' and forget all known projects that don’t exist anymore. WARNING: this function may take a while; go and grab a coffee"]))

  ;; Add to the main menu bar before the "Options" menu
  (define-key-after
    (lookup-key global-map [menu-bar])
    [elementaryx-project-menu]
    (cons "Project" elementaryx-project-menu)
    'file)

  :config

  ;; Bind elemenatryx-update-project-roots to `C-x p U'
  (define-key project-prefix-map "U" #'elementaryx-update-project-roots)

  ;; Set up project marks to consult them
  (defun elementaryx-collect-marks-from-buffer (buffer)
    "Collect all marks from BUFFER."
    (with-current-buffer buffer
      (let ((marks (cl-remove-duplicates
                    (cons (mark-marker) mark-ring)
                    :test #'equal)))
	marks)))
  (defun elementaryx-project-mark-ring ()
    "Return the project mark ring with marks from all buffers in the current project."
    (let ((project-buffers (project-buffers (project-current t)))
          (project-mark-ring nil))
      (dolist (buffer project-buffers)
	(message "Hello" buffer)
	(setq project-mark-ring
              (append project-mark-ring
                      (elementaryx-collect-marks-from-buffer buffer))))
      project-mark-ring))
  (defun elementaryx-consult-project-marks ()
    "Use `consult` to select a mark from the current project's mark ring."
    (interactive)
    (consult-global-mark (elementaryx-project-mark-ring)))

  ;; -- refine project switch command --

  (defun elementaryx-abort-project-switch () (interactive) (message "Abort Project Switch"))

  ;; Remove vc-dir (we will use magit) and project-find-regexp (we will use consult-ripgrep) from the project switch command
  (setq project-switch-commands
      (assq-delete-all 'project-vc-dir
		       (assq-delete-all 'project-find-regexp project-switch-commands)))
  ;; Add new functionalities to project switch command
  (define-key project-prefix-map "@" #'elementaryx-consult-project-marks)
  (add-to-list 'project-switch-commands '(elementaryx-consult-project-marks "Consult Marks")) ;; Add elementaryx-consult-project-mark
  (define-key project-prefix-map "b" #'consult-project-buffer)
  (add-to-list 'project-switch-commands '(consult-project-buffer "Buffers")) ;; Add consult-project-buffer ripgrep at the top of the list
  (define-key project-prefix-map "g" #'consult-ripgrep) ;; Originally: project-find-regexp
  (add-to-list 'project-switch-commands '(consult-ripgrep "(rip)Grep")) ;; Add consult-ripgrep at the top of the list
  ;; (define-key project-prefix-map "v" #'multi-vterm-project) ;; TODO: does not open as is a terminal in the new project
  ;; (add-to-list 'project-switch-commands '(multi-vterm-project "Vterm terminal") t) ;; Add Set at the end (t) of the list
  (define-key project-prefix-map "T" #'treemacs-add-and-display-current-project-exclusively)
  (add-to-list 'project-switch-commands '(treemacs-add-and-display-current-project-exclusively "Treemacs Set") t) ;; Add Set at the end (t) of the list
  (define-key project-prefix-map "t" #'treemacs-add-and-display-current-project)
  (add-to-list 'project-switch-commands '(treemacs-add-and-display-current-project "Treemacs Add") t) ;; Add Treemacs Add at the end (t) of the list
  (define-key project-prefix-map "B" #'build-menu)
  ;; (add-to-list 'project-switch-commands '(build-menu "Build") t) ;; Add Build at the end (t) of the list
  (define-key project-prefix-map "m" #'magit-project-status)
  (add-to-list 'project-switch-commands '(magit-project-status "Magit") t) ;; Add Magit at the end (t) of the list
  (define-key project-prefix-map "q" #'elementaryx-abort-project-switch)
  (add-to-list 'project-switch-commands '(elementaryx-abort-project-switch "Quit menu") t) ;; Add Quit at the end (t) of the list
  (add-to-list 'project-switch-commands '(project-dired "Switch"))) ;; Add basic switch at the top of the list

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Eglot, the built-in LSP client for Emacs
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package eglot
  ;; Configure hooks to automatically turn-on eglot for selected modes
  ; :hook
  ; (((python-mode ruby-mode elixir-mode) . eglot))

  :init
  (which-key-add-key-based-replacements "C-c e" "eglot (lsp)")
  :bind (:map toggle-elementaryx-map
	  ("e" . eglot)
	  :map eglot-mode-map ;; https://joaotavora.github.io/eglot/#index-eglot_002dmode_002dmap
	  ("C-c e q" . eglot-shutdown)
	  ("C-c e Q" . eglot-shutdown-all)
	  ("C-c e R" . eglot-reconnect)
	  ("C-c e a" . eglot-code-actions)
          ("C-c e f" . eglot-format)
	  ("C-c e r" . eglot-rename)
          ("C-c e o" . eglot-code-action-organize-imports)
          ("C-c e h" . eldoc)
          ("<f6>" . xref-find-definitions))

  :custom
  (eglot-send-changes-idle-time 0.1)
  (eglot-extend-to-xref t)              ; activate Eglot in referenced non-project files

  :config
  (fset #'jsonrpc--log-event #'ignore)  ; massive perf boost---don't log every event
  ;; Sometimes you need to tell Eglot where to find the language server
  ; (add-to-list 'eglot-server-programs
  ;              '(haskell-mode . ("haskell-language-server-wrapper" "--lsp")))

  (easy-menu-define
    elementaryx-eglot-map
    eglot-mode-map
    "Menu for Eglot (Language Server Support (LSP))."
    '("Eglot (LSP)"
       ["Find Definitions (Xref)" xref-find-definitions :help "Find the definition of the identifier at point."]
       ["Code Actions" eglot-code-actions :help "Find LSP code actions."]
       ["Format" eglot-format :help "Format Region of Buffer."]
       ["Rename" eglot-rename :help "Rename the current symbol."]
       ;; ["Organize Imports" eglot-code-action-organize-imports]
       ["Document (Eldoc)" eldoc :help "Document thing at point."]
       "----"
       ["Reconnect" eglot-reconnect :help "Reconnect to language server."]
       ["Shutdown" eglot-shutdown :help "Politely ask language server to quit."]
       ["Shutdown All" eglot-shutdown-all :help "Politely ask all language servers to quit, in order."]))

  ;; ;; Add the menu to the menu-bar
  ;; (define-key-after
  ;;   (lookup-key global-map [menu-bar])
  ;;   [elementaryx-eglot-map]
  ;;   (cons "Eglot (LSP)" elementaryx-eglot-map)
  ;;   'tools)

  ;; Add the menu to the context-menu (right-click)
  (define-key context-menu-mode-map [menu-bar elementaryx-eglot-map] nil))

;; Wait for https://issues.guix.gnu.org/70211 to be resolved:
;; consult-eglot requires emacs-consult-eglot guix package that comes
;; with emacs-eglot guix package which is currently broken. We stay
;; with built-in emacs eglot for now.
;; (use-package consult-eglot
;;   ;; :after (consult eglot)
;;   :commands (elementaryx-consult-eglot-symbols)  ;; Declare the function to defer loading
;;   ;; :bind (:map elementaryx-eglot-managed-mode-map  ;; Use :map to define keybinding for `eglot` buffers
;;   ;;             ("M-s ." . elementaryx-consult-eglot-symbols)) ;; orig. isearch-forward-symbol-at-point
;;   :hook (eglot-managed-mode . elementaryx-setup-consult-eglot-keybinding)  ;; Use :hook to trigger keybinding setup
;;   :config
;;   ;; We redefine consult-eglot-symbols to start with current symbol at
;;   ;; point
;;   (defun elementaryx-consult-eglot-symbols ()
;;     "Search for a matching line forward."
;;     (interactive)
;;     ;; TODO: move these `require' to the baseline of the `:config' (investigate why not straightforward)
;;     (require 'consult)
;;     (require 'eglot)
;;     (consult-eglot-symbols (thing-at-point 'symbol)))
;;   ;; Define a function to set the keybinding only when `eglot` is active in the buffer
;;   (defun elementaryx-setup-consult-eglot-keybinding ()
;;     "Set keybinding for `consult-eglot-symbols` when `eglot` is active."
;;     (local-set-key (kbd "M-s .") #'elementaryx-consult-eglot-symbols)))

;; (use-package consult-eglot-embark ;; provided by consult-eglot guix package
;;   :after (consult eglot embark)
;;   consult-eglot-embark-mode)

;; Eglot has built-in interaction with Flymake. Unfortunately Flymake
;; currently indicates errors in the fringe in Emacs 29, which is not
;; supported in terminal mode. This will change in Emacs 30.1 where
;; they will be automatically handled in the margin when running in a
;; terminal.
;; https://github.com/emacs-mirror/emacs/blob/6a68334d54667f78879deb2b03676e4c28d7a6a6/lisp/progmodes/flymake.el#L183

;; BEGIN PATCH
;; Note that "Find Definition" (through `xref-find-definitions-at-mouse') and "Find references" when handled
;; by `eglot' does not take the symbol under right-click but the symbol under cursor.
;; This is solved in the master branch of emacs:
;; https://lists.gnu.org/archive/html/bug-gnu-emacs/2023-08/msg02336.html
;; Meanwhile, we locally redefine `xref-find-definitions-at-mouse'

(use-package xref
  :defer t
  :config
  (message "Temporary patch: redefine xref-find-definitions/references-at-mouse with master branch definitions")
  (defun xref-find-definitions-at-mouse (event)
    "Find the definition of identifier at or around mouse click.
This command is intended to be bound to a mouse event."
    (interactive "e")
    (let ((identifier
           (save-excursion
             (mouse-set-point event)
             (xref-backend-identifier-at-point (xref-find-backend)))))
      (if identifier
          (progn
            (mouse-set-point event)
            (xref-find-definitions identifier))
	(user-error "No identifier here"))))

;;;###autoload
  (defun xref-find-references-at-mouse (event)
    "Find references to the identifier at or around mouse click.
This command is intended to be bound to a mouse event."
    (interactive "e")
    (let ((identifier
           (save-excursion
             (mouse-set-point event)
             (xref-backend-identifier-at-point (xref-find-backend)))))
      (if identifier
          (let ((xref-prompt-for-identifier nil))
            (mouse-set-point event)
            (xref-find-references identifier))
	(user-error "No identifier here")))))

;; END PATCH
;; TODO Remove the above patch when we get the proper release of emacs

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;                                        ;;;
;;; Build (CMake, Bazel, Make, Ninja, ...) ;;;
;;;                                        ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; https://github.com/27justin/build.el
(use-package build
  :bind (("C-c t B" . build-menu)))

;;;;;;;;;;;;;;;;;;;;;
;;;               ;;;
;;; Editor Config ;;;
;;;               ;;;
;;;;;;;;;;;;;;;;;;;;;

;; https://github.com/editorconfig/editorconfig-emacs
(use-package editorconfig
  :config
  (editorconfig-mode 1))

;;;;;;;;;;;;;;;;;;;;
;;; Clang-format ;;;
;;;;;;;;;;;;;;;;;;;;

;; ;; c-c++ https://www.spacemacs.org/layers/+lang/c-c++/README.html
;; ;; Bind clang-format-region to C-M-tab in all modes:
;; (global-set-key [C-M-tab] 'clang-format-region)
;; ;; Bind clang-format-buffer to tab on the c++-mode only:
;; (add-hook 'c++-mode-hook 'clang-format-bindings)
;; (defun clang-format-bindings ()
;;   (define-key c++-mode-map [tab] 'clang-format-buffer))


(provide 'elementaryx-dev-minimal)
